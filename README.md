# 1º projeto de Segurança de Redes e Sistemas de Computadores
De momento, apenas a fase 1 está completamente funcional (a fase dois está testing).

# Clonagem
Para efetuar a clonagem, bastará clicar no botão "Clone" no separador "Source", copiar o comando e corrê-lo no terminal.
Todos os ficheiros necessários para o teste do programa encontram-se na subdiretoria "Resources". 
Pode trocar pelos seus caso deseje.

# Execução do programa
Para a execução do código, primeiro certifique-se que se encontra na subdiretoria "src/" e execute o comando:

# java -Djava.net.preferIPv4Stack=true SecureChat.SecureChat <username> <ip-multicast> 9000 <password_da_keystore>

sendo de notar que se não trocar o ficheiro mykeystore.jks o comando a executar será

# java -Djava.net.preferIPv4Stack=true SecureChat.SecureChat <username> 224.10.10.10 9000 ola123

