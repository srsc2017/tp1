package AuthenticationServer;

import STGC.STGCServerSocket;

import java.io.*;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.util.*;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class AuthenticationServer {

    private static final String ADDRESS = "224.10.10.10";
    private static final int PORT = 9000;
    private static final String CIPHERSUITE_PATH = "AuthenticationServer/Resources/ciphersuite.conf",
            KEYSTORE_PATH = "AuthenticationServer/Resources/mykeystore.jks",
            DAC_PATH = "AuthenticationServer/Resources/dacl.conf";


    private static STGCServerSocket socket;

    private static Map<String, List<String>> accessList;
    private static Map<String, List<Integer>> nonceHistory;
    private static Map<String, String> userCredentials;

    private static final String LOGGER_PATH = "AuthenticationServer/Resources/logger2.log";
    private static Logger logger = Logger.getLogger("InfoLogging");
    private static FileHandler fh;

    public static void main(String[] args) throws IOException {
        userCredentials = new HashMap<>();
        userCredentials.put("rmgl/ricardo@gmail.com", "���[��\u000B�S\u0006hWE������n�-\u0016�K�S���\u0004\u0013��\u001F�\u001B\u0016��u��)�\u000F�Cf ���\u0004�xD9��*�\u000F");

        fh = new FileHandler(LOGGER_PATH);
        logger.addHandler(fh);
        SimpleFormatter formatter = new SimpleFormatter();
        fh.setFormatter(formatter);

        socket = new STGCServerSocket(PORT);
        socket.joinGroup(InetAddress.getByName(ADDRESS));

        //receive STGCSocket messages
        DatagramPacket packet;
        byte[] data, buffer;
        System.out.println("hey");
        while(true){
            buffer = new byte[65508];
            packet = new DatagramPacket(buffer, buffer.length);
            socket.receive(packet);//blocking
            InetAddress senderAddress = packet.getAddress();
            int senderAddressPort = packet.getPort();
            System.out.println("Received packet from "+senderAddress+":"+senderAddressPort);

            //extracting info
            data = packet.getData();

            DataInputStream dataStream = new DataInputStream(new ByteArrayInputStream (data));
            String clientId = dataStream.readUTF();
            String[] tokens = clientId.split(":");
            String user = tokens[0], email = tokens[1];
            int nonceC = dataStream.readInt();
            String ipmc = dataStream.readUTF(), password = dataStream.readUTF();

            dataStream.close();

            //validating and preparing response accordingly
            byte[] destination = prepareResponse(validateJoinRequest(user, email, nonceC, ipmc, password), nonceC);
            packet.setData(destination);
            packet.setAddress(senderAddress);
            packet.setPort(senderAddressPort);
            //send message
            socket.send(packet, clientId, nonceC+1);
            System.out.println("oi");

        }
    }

    private static short validateJoinRequest(String username, String email, int nonceC, String ipmc, String password) throws IOException{
        //check his nonces
        List<Integer> previousNonces = nonceHistory.get(username+email);
        if (previousNonces == null) {//no previous nonces -> new list and adding nonce to it

            previousNonces = new LinkedList<>();
            previousNonces.add(nonceC);
            nonceHistory.put(username+email, previousNonces);

        }else{

            if (!previousNonces.contains(nonceC))//valid nonce
                previousNonces.add(nonceC);
            else return -1; //invalid nonce -> replay attack -> ignore packet

        }

        //fetch answers - first check if user has access to IPMC, then check credentials
        List<String> access = accessList.get(ipmc);
        if (access != null){
            if (!access.contains(username))
                return 1;

            String localPassword = userCredentials.get(username+email);
            if (localPassword == null)
                return 3;

            if (!localPassword.equals(password))
                return 2;

            return 0;
        }else return 4;//ipmc does not exist
    }

    //0 is OK, 1 is "access to address not permitted", 2 is "wrong password"
    private static byte[] prepareResponse(short opCode, int nonceC) throws IOException{
        //data to be sent: NonceC+1 || NonceS || TicketAS
        switch(opCode){
            case 0:
                int nonceS = new Random().nextInt((1000000) + 1);
                byte[] ticketAS = makeTicket(), info = new byte[ticketAS.length + 4 + 2];//int + short bytes = 6
                DataOutputStream dataStream = new DataOutputStream(new ByteArrayOutputStream());
                dataStream.writeInt(nonceC + 1);
                dataStream.writeInt(nonceS);
                dataStream.write(ticketAS);
                dataStream.close();

                return info;
            //Nestes casos, devolver what?
            case 1:
                logger.info("User with no permission tried to login.");
                break;
            case 2:
                logger.info("User with wrong password tried to login");
                break;
            case 3:
                logger.info("User that doesnt exist tried to login.");
                break;
            default:
                //some kind of attack was made, ignore
        }
        return null;
    }

    private static byte[] makeTicket(){

        try {
            File ciphersuite = new File(CIPHERSUITE_PATH);
            File keystore = new File(KEYSTORE_PATH);
            byte[] buf = new byte[new Long(ciphersuite.length()).intValue()];
            ByteArrayOutputStream toSend = new ByteArrayOutputStream();
            DataInputStream file = null;
            file = new DataInputStream(new FileInputStream(ciphersuite));

            //writing the files' name and length
            toSend.write(ciphersuite.getName().getBytes());
            toSend.write(new Long(ciphersuite.length()).byteValue());
            toSend.write(keystore.getName().getBytes());
            toSend.write(new Long(keystore.length()).byteValue());

            //ciphersuite
            file.readFully(buf);
            file.close();
            toSend.write(buf);

            //keystore
            buf = new byte[new Long(keystore.length()).intValue()];
            file = new DataInputStream(new FileInputStream(keystore));
            file.readFully(buf);
            file.close();
            toSend.write(buf);

            //returning
            toSend.close();
            return toSend.toByteArray();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
