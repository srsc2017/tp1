package STGC;

import java.io.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.nio.ByteBuffer;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.util.*;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import javax.crypto.*;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class STGCServerSocket extends STGCSocket{

    private static final String USERS_PATH = "AuthenticationServer/Resources/users.conf";

    byte[] salt = new byte[] { 0x7d, 0x60, 0x43, 0x5f, 0x02, (byte)0xe9, (byte)0xe0, (byte)0xae };
    int iterationCount = 2048;

    public STGCServerSocket(int port) throws IOException{
        super(port);
    }

    //PBE Encryption
    private byte[] makePayload(byte[] payload, String password, int nonceC){
        try {
            //first make mack(X)
            ByteBuffer bb = ByteBuffer.allocate(Integer.SIZE / 8 + password.getBytes().length);
            bb.putInt(nonceC);
            bb.put(password.getBytes());
            MessageDigest hash = MessageDigest.getInstance("MD5", DEFAULT_PROVIDER);
            byte[] macKpass = hash.digest(bb.array());


            Mac mack = Mac.getInstance("HMacSHA1", DEFAULT_PROVIDER);
            Key macKKey = new SecretKeySpec(macKpass, "HMacSHA1");
            mack.init(macKKey);
            byte[] macX = mack.doFinal(payload);

            //toSend = header + data
            byte[] destination = new byte[macX.length + payload.length];
            System.arraycopy(payload, 0, destination, 0, payload.length);
            System.arraycopy(macX, 0, destination, payload.length, macX.length);

            //encrypt everything
            cipher = Cipher.getInstance("PBEWITHSHA256AND192BITAES-CBC-BC", DEFAULT_PROVIDER);

            bb = ByteBuffer.allocate(Integer.SIZE / 8 + password.getBytes().length);
            bb.put(password.getBytes());
            bb.putInt(nonceC);
            Key key = new SecretKeySpec(bb.array(), "PBEWITHSHA256AND192BITAES-CBC-BC");
            cipher.init(Cipher.ENCRYPT_MODE, key, new PBEParameterSpec(salt, iterationCount));

            return cipher.doFinal(destination);

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        }

        return null;
    }

    //PBE Encryption
    private byte[] unlockPayload(byte[] data, String clientID) throws NoSuchPaddingException, NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException, IOException, InvalidKeySpecException, InvalidAlgorithmParameterException {
        BufferedReader users = new BufferedReader(new FileReader(USERS_PATH));

        String[] userTokens;

        do{
          userTokens = users.readLine().split(" ");
        }while(!userTokens[0].equals(clientID));

        MessageDigest md = MessageDigest.getInstance("SHA-512", DEFAULT_PROVIDER);

        byte[] password = md.digest(userTokens[1].getBytes());

        PBEKeySpec pbeSpec = new PBEKeySpec(new String(password).toCharArray());
        SecretKeyFactory keyFact = SecretKeyFactory.getInstance("PBEWITHSHA256AND192BITAES-CBC-BC", DEFAULT_PROVIDER);//never used

        Key key = keyFact.generateSecret(pbeSpec);
        Cipher cipher = Cipher.getInstance("PBEWITHSHA256AND192BITAES-CBC-BC", DEFAULT_PROVIDER);

        cipher.init(Cipher.DECRYPT_MODE, key, new PBEParameterSpec(salt, iterationCount));
        byte[] rawdata = cipher.doFinal(data);

        DataInputStream dataStream = new DataInputStream(new ByteArrayInputStream(rawdata));

        int nonceC = dataStream.readInt();
        String ipmc = dataStream.readUTF();
        byte[] hashedPassword = new byte[password.length];
        dataStream.read(hashedPassword);

        byte[] mackX = new byte[data.length - 4 - ipmc.getBytes().length - hashedPassword.length];
        dataStream.read(mackX);


        ByteBuffer bb = ByteBuffer.allocate(Integer.SIZE / 8 + hashedPassword.length);
        bb.putInt(nonceC);
        bb.put(hashedPassword);

        MessageDigest hash = MessageDigest.getInstance("MD5", DEFAULT_PROVIDER);
        byte[] macKpass = hash.digest(bb.array());

        Mac mack = Mac.getInstance("HMacSHA1", DEFAULT_PROVIDER);

        Key macKKey = new SecretKeySpec(macKpass, "HMacSHA1");
        mack.init(macKKey);

        mack.update(ByteBuffer.allocate(4).putInt(nonceC).array());
        mack.update(ipmc.getBytes());
        mack.update(hashedPassword);

        if (!MessageDigest.isEqual(mack.doFinal(), mackX)) {
            //logger.info("System message failed tempering test!");
            return null;
        }


        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        DataOutputStream dos = new DataOutputStream(bos);
        dos.writeUTF(clientID);
        dos.writeInt(nonceC);
        dos.writeUTF(ipmc);
        dos.close();

        return bos.toByteArray();
    }

    public void send(DatagramPacket packet, String clientID, int nonceC)  {
        //cipher payload
        byte[] data = packet.getData();
        data = makePayload(data, clientID, nonceC);

        //make STGC header
        byte[] header = makeHeader(SYSTEM_PAYLOAD, new Integer(data.length).shortValue());

        //toSend = header + data
        byte[] destination = new byte[header.length + data.length];
        System.arraycopy(header, 0, destination, 0, header.length);
        System.arraycopy(data, 0, destination, header.length, data.length);

        //sending
        packet.setData(destination);
        System.out.println("Sending packet...");
        try {
            DatagramSocket sock = new DatagramSocket();
            sock.send(packet);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void receive(DatagramPacket packet){
        //only accepts valid packets
        try {
            String clientId, ipmc;
            int nonceC;
            byte[] authenticatorC = null;
            boolean valid = false;

            do {
                super.receive(packet);
                System.out.println("Received packet -> Processing");

                byte[] data = removeHeader(packet.getData());

               /* if (validateHeader(data[0])){
                    System.out.println("Invalid header -> discarding");
                    continue; //invalid header
                }*/

                //reading clientId, nonceC, impc and authenticatorC(encrypted) from header
                DataInputStream dataStream = new DataInputStream(new ByteArrayInputStream(data));
                clientId = dataStream.readUTF();
                nonceC = dataStream.readInt();
                ipmc = dataStream.readUTF();
                authenticatorC = new byte[data.length - clientId.length() - ipmc.length()];
                dataStream.read(authenticatorC);

                System.out.println("Attempting to unlock payload");
                authenticatorC = unlockPayload(authenticatorC, clientId);
                DataInputStream di = new DataInputStream(new ByteArrayInputStream(authenticatorC));
                System.out.println(di.readUTF());
                if (authenticatorC != null) valid = true;

            }while(!valid);//while packet is invalid

            packet.setData(authenticatorC);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        }

    }

    private boolean validateHeader(byte[] header){

        if (header[0] != VERSION)
            return false;

        if (header[2] != SYSTEM_PAYLOAD)
            return false;

        return true;
    }



}
