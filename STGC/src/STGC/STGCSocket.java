package STGC;

import javax.crypto.Cipher;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.MulticastSocket;
import java.nio.ByteBuffer;

public abstract class STGCSocket extends MulticastSocket {

    public static final byte VERSION = 0b00010001, SEPARATOR = 0x00,
            SYSTEM_PAYLOAD = 'S', MESSAGE_PAYLOAD = 'M';

    public static final String DEFAULT_PROVIDER = "BC";
    public static final int HISTORY = 20, HEADER_SIZE = 6;
    public Cipher cipher;

    public STGCSocket(int port) throws IOException {
        super(port);
    }

    protected byte[] makeHeader(byte payload_type, short payload_size){
        ByteBuffer header = ByteBuffer.allocate(6);

        header.put(VERSION);
        header.put(SEPARATOR);
        header.put(payload_type);
        header.put(SEPARATOR);
        header.putShort(payload_size);

        return header.array();
    }

    protected byte[] removeHeader(byte[] data){
        byte version = data[0], type = data[2];

        short length = (short) (data[4] << 8 | data[5]);

        byte[] payload = new byte[length];

        System.arraycopy(data, HEADER_SIZE, payload, 0, length);

        return payload;
    }

    public void send(DatagramPacket packet) throws IOException {
        super.send(packet);
    }

    public void receive(DatagramPacket packet) throws IOException {
        super.receive(packet);
    }
}
