package STGC;

import java.io.*;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.security.cert.CertificateException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class STGCClientSocket extends STGCSocket {

    private static final String CIPHERSUITE_PATH = "Resources/ciphersuite.conf",
            INIT_PATH = "AuthenticationServer/Resources/Init.conf",
            KEYSTORE_PATH = "Resources/mykeystore.jks",
            LOGGER_PATH = "STGC/logger2.log",
            DEFAULT_PROVIDER = "BC",
            KEYSTORE_TYPE = "JCEKS",
            RESOURCES = "AuthenticationServer/Resources/";
    private static final int HISTORY = 20, HEADER_SIZE = 6;

    private Logger logger = Logger.getLogger("InfoLogging");
	/*
      IDs will be established as identifiers in the form:
      "<userName>:<RFC 822 Email-Address>"
	 */

    byte[] salt = new byte[] { 0x7d, 0x60, 0x43, 0x5f, 0x02, (byte)0xe9, (byte)0xe0, (byte)0xae };
    int iterationCount = 2048;

    private Key algorithmKey, macKM, macKA;
    private Cipher cipher;
    private Mac mac;
    private KeyStore keyStore;
    private byte[] ivBytes;
    private String algorithm, macAlgorithm, userName;
    private Map<String, List<Long>> nonceHistory;
    private InetAddress chatGroup;
    private FileHandler fh;
    private int port;


    public STGCClientSocket(String serverIP, int port, String password, InetAddress group, String userName) throws
            IOException{

        super(port);
        super.joinGroup(InetAddress.getByName(serverIP));
        this.userName = userName;
        nonceHistory = new HashMap<>();
        chatGroup = group;
        this.port = port;

        //initLogger();

        initAuthentication(password);

		/*comment this line for debug
		logger.setUseParentHandlers(false);
		 */
    }

    private void initLogger() throws IOException {
        fh = new FileHandler(LOGGER_PATH);
        logger.addHandler(fh);
        SimpleFormatter formatter = new SimpleFormatter();
        fh.setFormatter(formatter);
    }

    private void initKeys(char[] password) throws IOException {

        try {
            keyStore = KeyStore.getInstance(KEYSTORE_TYPE);

            FileInputStream stream =
                    new FileInputStream(KEYSTORE_PATH);
            keyStore.load(stream, password);
            stream.close();

            algorithmKey = keyStore.getKey(chatGroup.getHostName() + "algorithm",
                    password);
            macKM = keyStore.getKey(chatGroup.getHostName() + "macKM", password);
            macKA = keyStore.getKey(chatGroup.getHostName() + "macKA", password);

            //mucking and discarding password after use
            for (char c : password) c = (char) (126 * Math.random());
            password = null;

            //mucking and discarding keyStore
            keyStore = null;
        } catch (KeyStoreException | FileNotFoundException e) {
            logger.info("Keystore does not exist.");
        } catch (NoSuchAlgorithmException e) {
            logger.info("No such algorithm.");
        } catch (CertificateException e) {
            logger.info("Error loading certificate.");
        } catch (UnrecoverableKeyException e) {
            logger.info("Error loading key.");
        }

    }

    private void initKeyParameters() throws IOException{

        try{
            BufferedReader br = new BufferedReader(
                    new FileReader(CIPHERSUITE_PATH)); //just doesnt work


            while (!(br.readLine()).equals("<" + chatGroup.getHostName() + ">")) ;

            algorithm = br.readLine().split(" ")[1];
            cipher = Cipher.getInstance(algorithm, DEFAULT_PROVIDER);
            String[] tokens = br.readLine().split(" ");
            ivBytes = new byte[tokens.length-1];
            for (int i = 1; i < tokens.length; i++)
                ivBytes[i - 1] = Byte.valueOf(tokens[i]); //falta cast to byte
            br.readLine();
            br.readLine();

            macAlgorithm = br.readLine().split(" ")[1];
            mac = Mac.getInstance(macAlgorithm, DEFAULT_PROVIDER);

            br.close();
        }catch(NoSuchAlgorithmException e ){
            logger.info("Invalid algorithm.");
        }catch(NoSuchProviderException e){
            logger.info("Invalid security provider.");
        }catch(NoSuchPaddingException e){
            logger.info("Invalid padding.");
        }
    }

    private void initAuthentication(String cs){
        try {
            //this part can be condensed in a method
            int nonceC = new Random().nextInt((1000000) + 1);
            String ipmc = chatGroup.getHostName();

            MessageDigest md = MessageDigest.getInstance("SHA-512", DEFAULT_PROVIDER);

            byte[] passwordDigested = md.digest(cs.getBytes());

            PBEKeySpec pbeSpec = new PBEKeySpec(new String(passwordDigested).toCharArray());

            SecretKeyFactory keyFact = SecretKeyFactory.getInstance("PBEWITHSHA256AND192BITAES-CBC-BC", DEFAULT_PROVIDER);

            Key algorithmKey = keyFact.generateSecret(pbeSpec);

            ByteBuffer bb = ByteBuffer.allocate(Integer.SIZE / 8 + passwordDigested.length);
            bb.putInt(nonceC);
            bb.put(passwordDigested);

            MessageDigest hash = MessageDigest.getInstance("MD5", DEFAULT_PROVIDER);
            byte[] macKpass = hash.digest(bb.array());


            Mac mack = Mac.getInstance("HMacSHA1", DEFAULT_PROVIDER);

            //enviar o hash da password + nonce para randomness
            Key macKKey = new SecretKeySpec(macKpass, "HMacSHA1");
            mack.init(macKKey);

            ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
            DataOutputStream outDataStream = new DataOutputStream(byteStream);

            outDataStream.writeInt(nonceC);
            outDataStream.writeUTF(ipmc);
            outDataStream.write(passwordDigested);
            outDataStream.close();
            byte[] x = byteStream.toByteArray();

            byte[] macKX = mack.doFinal(x);

            Cipher cEnc = Cipher.getInstance("PBEWITHSHA256AND192BITAES-CBC-BC", DEFAULT_PROVIDER);
            cEnc.init(Cipher.ENCRYPT_MODE, algorithmKey, new PBEParameterSpec(salt, iterationCount));

            cEnc.update(x);

            byte[] autenticatorC = cEnc.doFinal(macKX);

            ByteArrayOutputStream toSend = new ByteArrayOutputStream();
            outDataStream = new DataOutputStream(toSend);

            outDataStream.writeUTF(userName);
            outDataStream.writeInt(nonceC);
            outDataStream.writeUTF(ipmc);
            outDataStream.write(autenticatorC);
            outDataStream.close();

            byte[] data = toSend.toByteArray();

            ByteArrayOutputStream payload = new ByteArrayOutputStream();
            outDataStream = new DataOutputStream(payload);

            outDataStream.write(makeHeader(SYSTEM_PAYLOAD, new Integer(data.length).shortValue()));
            outDataStream.write(data);
            outDataStream.close();

            data = payload.toByteArray();

            //criar packet
            DatagramPacket sendPacket = new DatagramPacket(data, data.length, chatGroup, 9000);
            super.send(sendPacket);

            System.out.println("Sending");
            byte[] buffer = new byte[65508];
            DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
            receive(packet);
            System.out.println("Received");

            //this part can be condensed in a method
            short length = (short) (data[4] << 8 | data[5]);

            byte[] header = new byte[HEADER_SIZE];
            byte[] payloadR = new byte[length];
            System.arraycopy(data, 0, header, 0, HEADER_SIZE);
            System.arraycopy(data, HEADER_SIZE, payloadR, 0, length);

            nonceC++;
            byte[] nonceBytes = ByteBuffer.allocate(4).putInt(nonceC).array();
            byte[] dpass = new byte[Integer.SIZE / 8 + passwordDigested.length];
            System.arraycopy(passwordDigested, 0, dpass, 0, passwordDigested.length);
            System.arraycopy(nonceBytes, passwordDigested.length, dpass, 0, 4);

            Key decryptKey = new SecretKeySpec(dpass, "PBEWITHSHA256AND192BITAES-CBC-BC");
            cEnc.init(Cipher.DECRYPT_MODE, decryptKey);

            payloadR = cEnc.doFinal(payloadR);

            mack.init(macKKey);

            ByteArrayInputStream load = new ByteArrayInputStream(payloadR);
            DataInputStream inDataStream = new DataInputStream(load);

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            DataOutputStream dos = new DataOutputStream(bos);

            int nonceC1 = inDataStream.readInt();
            dos.writeInt(nonceC1);

            int nonceS = inDataStream.readInt();
            dos.writeInt(nonceS);

            String fileName = inDataStream.readUTF();
            dos.writeUTF(fileName);

            int fileSize = inDataStream.readInt();
            dos.writeInt(fileSize);

            byte[] file1 = new byte[fileSize];
            inDataStream.read(file1);

            String fileName2 = inDataStream.readUTF();
            dos.writeUTF(fileName2);

            int fileSize2 = inDataStream.readInt();
            dos.writeInt(fileSize2);

            byte[] file2 = new byte[fileSize2];
            dos.write(file2);

            dos.close();

            byte[] macBytes = new byte[payloadR.length- 8-fileName.getBytes().length - fileSize - fileName2.getBytes().length - fileSize2 - 8];
            inDataStream.read(macBytes);// dar tamanho

            if (!MessageDigest.isEqual(mac.doFinal(bos.toByteArray()), macBytes)) {
                logger.info("System message failed tempering test!");
                System.exit(1); //panic
            }

            System.out.println("Writing authentication files to disk.");
            FileOutputStream out = new FileOutputStream("Resources/" + fileName);
            out.write(file1);
            out.close();

            out = new FileOutputStream("Resources/" + fileName2);
            out.write(file2);
            out.close();

            initKeyParameters();
            initKeys(cs.toCharArray());
        } catch (NoSuchAlgorithmException e1) {
            e1.printStackTrace();
        } catch (NoSuchProviderException e1) {
            e1.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        }
    }

    //for normal packets i.e. MESSAGE HEADERS (M)
    //TODO: DEVIAMOS USAR O PACKET.SETDATA PARA NAO FODER O ADDRESS DESTINO DO PACOTE (CASO EXISTA)
    public void send(DatagramPacket packet) throws IOException {

        //header stuff
        byte[] payload = makePayload(packet.getData());
        byte[] header = makeHeader(MESSAGE_PAYLOAD,
                new Integer(payload.length).shortValue());
        byte[] destination = new byte[header.length + payload.length];

        System.arraycopy(header, 0, destination, 0, header.length);
        System.arraycopy(payload, 0, destination, header.length, payload.length);

        packet.setData(destination);
        super.send(packet);
    }

    //for STGC system use ONLY i.e. SYSTEM HEADERS (S)
    private void sendSystemPacket(DatagramPacket packet) throws IOException {

        super.send(packet);
    }

    public void receive(DatagramPacket packet) throws IOException {
        boolean flag = true;
        do {
            super.receive(packet);

            //TODO: filtrar header do pacote
            byte[] data = packet.getData();
            byte version = data[0], type = data[2];

            if(version != VERSION) {
                logger.info("Ignoring a package from an outdated client!");
                continue;
            }

            short length = (short) (data[4] << 8 | data[5]);

            byte[] payload = new byte[length];

            System.arraycopy(data, HEADER_SIZE, payload, 0, length);

            payload = unlockPayload(payload);
            if(payload == null) {
                logger.info("Survived an invalid package");
                continue;
            }
            flag = false;
            packet.setData(payload);
        }while(flag);
    }



    private byte[] makePayload(byte[] data) throws IOException {
        try {
            cipher.init(Cipher.ENCRYPT_MODE, algorithmKey,
                    new IvParameterSpec(ivBytes));
            mac.init(macKM);

            ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
            DataOutputStream dataStream = new DataOutputStream(byteStream);

            dataStream.writeUTF(userName);
            long nonceSent = System.currentTimeMillis() * (int)( Math.random() * 100);
            dataStream.writeLong(nonceSent);
            dataStream.write(data, 0, data.length);
            dataStream.close();

            byte[] mp = byteStream.toByteArray();
            byte[] mpMac = mac.doFinal(mp);

            byteStream = new ByteArrayOutputStream();
            dataStream = new DataOutputStream(byteStream);
            dataStream.write(mp, 0, mp.length);
            dataStream.write(mpMac, 0, mpMac.length);
            dataStream.close();
            byte[] c = byteStream.toByteArray();

            mac.init(macKA);

            byte[] cCipher = cipher.doFinal(c);
            byte[] cMac = mac.doFinal(cCipher);

            byteStream = new ByteArrayOutputStream();
            dataStream = new DataOutputStream(byteStream);
            dataStream.write(cCipher, 0, cCipher.length);
            dataStream.write(cMac, 0, cMac.length);
            dataStream.close();

            return byteStream.toByteArray();

        } catch (InvalidKeyException e) {
            logger.info("Invalid key.");
        } catch (InvalidAlgorithmParameterException e) {
            logger.info("Bad parameters.");
        } catch (IllegalBlockSizeException e) {
            logger.info("Invalid block size.");
        } catch (BadPaddingException e) {
            logger.info("Invalid padding.");
        }

        System.exit(1);
        return null;

    }

    private byte[] unlockPayload(byte[] data) throws IOException {
        try {
            cipher.init(Cipher.DECRYPT_MODE, algorithmKey,
                    new IvParameterSpec(ivBytes));

            mac.init(macKA);

            byte[] payload = new byte[data.length - mac.getMacLength()];
            byte[] payloadMac = new byte[mac.getMacLength()];

            int offset = data.length - mac.getMacLength();
            System.arraycopy(data, 0, payload, 0, offset);
            System.arraycopy(data, offset, payloadMac, 0, mac.getMacLength());
            mac.update(payload);

            //testing C against MAC(C)
            if (!MessageDigest.isEqual(mac.doFinal(), payloadMac)) {
                logger.info("Message from failed first tempered test!");
                return null; //panic
            }

            payload = cipher.doFinal(payload);

            mac.init(macKM);

            int length = mac.getMacLength();
            offset = payload.length - length;
            byte[] message = new byte[offset];
            byte[] messageMac = new byte[length];

            System.arraycopy(payload, 0, message, 0, offset);
            System.arraycopy(payload, offset,
                    messageMac, 0, mac.getMacLength());
            mac.update(message);

            //testing MP against MAC(MP)
            if (!MessageDigest.isEqual(mac.doFinal(), messageMac)) {
                logger.info("Message failed second tempered test!");
                return null; //panic
            }


            DataInputStream dataStream = new DataInputStream(
                    new ByteArrayInputStream(message, 0, message.length));

            String userName = dataStream.readUTF();

            long nonce = dataStream.readLong();
            //stopping replay attacks
            List<Long> tmp = nonceHistory.get(userName);
            if (tmp == null) {
                tmp = new LinkedList<>();
                ((LinkedList<Long>) tmp).addFirst(nonce);
                nonceHistory.put(userName, tmp);
            } else {
                if (!tmp.contains(nonce)) {
                    if (tmp.size() > HISTORY) {
                        tmp.remove(0);
                        tmp.add(nonce);
                    }
                }else{
                    //else its a replay attack, so ignore
                    logger.info("Message from " + userName + " was duplicated!");
                    return null;
                }
            }
            //replay attacks foiled
            byte[] plainText = new byte[
                    message.length - userName.length() - Long.SIZE/8];

            dataStream.read(plainText);

            return plainText;

        } catch (InvalidKeyException e) {
            logger.info("Invalid key.");
        } catch (InvalidAlgorithmParameterException e) {
            logger.info("Bad parameters.");
        } catch (IllegalBlockSizeException e) {
            logger.info("Invalid block size.");
        } catch (BadPaddingException e) {
            logger.info("Invalid padding.");
        }

        System.exit(1);
        return null;
    }
}
