package Utilities;

import java.io.FileOutputStream;
import java.io.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

public class PasswordHasher{

  private static final String PATH = "users.conf";
  private static final String DEFAULT_PROVIDER = "BC";

  public static void main(String[] args) throws NoSuchAlgorithmException, NoSuchProviderException, IOException{

    MessageDigest md = MessageDigest.getInstance("SHA-512", DEFAULT_PROVIDER);
    byte[] passwordDigested = md.digest(args[1].getBytes());

    String hashedPWD = new String(passwordDigested);

    FileOutputStream out = new FileOutputStream(PATH);
    for (char c : args[0].toCharArray()) out.write(c);
    for (byte d : passwordDigested) out.write(d);
    out.write("\n".getBytes());


    BufferedReader br = new BufferedReader(new FileReader(PATH));
    String hey = br.readLine();
    for (byte d : hey.getBytes()) out.write(d);

  }
}
