package SecureChat;

import java.io.*;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.net.MulticastSocket;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;

public class STGCSocket extends MulticastSocket {

	private static final byte VERSION = 0b00010001, SEPARATOR = 0x00, MESSAGE_PAYLOAD = 'M';
	private static final String CIPHERSUITE_PATH = "Resources/ciphersuite.conf",
			KEYSTORE_PATH = "Resources/mykeystore.jks",
			DEFAULT_PROVIDER = "BC",
			KEYSTORE_TYPE = "JCEKS";
	private static final int HISTORY = 20, HEADER_SIZE = 6;

	private Key algorithmKey, macKM, macKA;
	private Cipher cipher;
	private Mac mac;
	private KeyStore keyStore;
	private byte[] ivBytes;
	private String algorithm, macAlgorithm;
	private Map<String, List<Long>> nonceHistory;
	private InetAddress chatGroup;

	public STGCSocket(int port, char[] password, InetAddress group) throws IOException{

		super(port);
		nonceHistory = new HashMap<>();
		chatGroup = group;

		initKeyParameters();
		initKeys(password);
	}

	private void initKeys(char[] password) throws IOException {

		try {
			keyStore = KeyStore.getInstance(KEYSTORE_TYPE);

			FileInputStream stream =
					new FileInputStream(KEYSTORE_PATH);
			keyStore.load(stream, password);
			stream.close();

			algorithmKey = keyStore.getKey(chatGroup.getHostName() + "algorithm", password);
			macKM = keyStore.getKey(chatGroup.getHostName() + "macKM", password);
			macKA = keyStore.getKey(chatGroup.getHostName() + "macKA", password);

			//mucking and discarding password after use
			for (char c : password) c = (char) (126 * Math.random());
			password = null;

			//discarding keyStore
			keyStore = null;

		} catch (KeyStoreException | FileNotFoundException e) {
			System.err.println("Erro a obter a KeyStore.");
		} catch (NoSuchAlgorithmException e) {
			System.err.println("Nao existe o algoritmo requisitado.");
		} catch (CertificateException e) {
			System.err.println("Erro ao carregar o certificado.");
		} catch (UnrecoverableKeyException e) {
			System.err.println("Erro ao carregar a chave");
		}

	}

	private void initKeyParameters() throws IOException{

		try{
			BufferedReader br = new BufferedReader(
					new FileReader(CIPHERSUITE_PATH));

			ivBytes = new byte[16];

			while (!(br.readLine()).equals("<" + chatGroup.getHostName() + ">")) ;

			algorithm = br.readLine().split(" ")[1];
			cipher = Cipher.getInstance(algorithm, DEFAULT_PROVIDER);
			String[] tokens = br.readLine().split(" ");
			for (int i = 1; i < tokens.length; i++)
				ivBytes[i - 1] = Byte.valueOf(tokens[i]);
			br.readLine();
			br.readLine();

			macAlgorithm = br.readLine().split(" ")[1];
			mac = Mac.getInstance(macAlgorithm, DEFAULT_PROVIDER);

			br.close();
		}catch(NoSuchAlgorithmException e ){
			System.err.println("Nao existe o algoritmo requisitado.");
		}catch(NoSuchProviderException e){
			System.err.println("Provedor de seguranca nao disponivel.");
		}catch(NoSuchPaddingException e){
			System.err.println("Mecanismo de preenchimento nao disponivel.");
		}
	}

	public void send(DatagramPacket packet, String username) throws IOException {

		//header stuff
		byte[] payload = makePayload(packet.getData(), username);
		byte[] header = makeHeader(MESSAGE_PAYLOAD, new Integer(payload.length).shortValue());
		byte[] destination = new byte[header.length + payload.length];

		System.arraycopy(header, 0, destination, 0, header.length);
		System.arraycopy(payload, 0, destination, header.length, payload.length);
		packet = new DatagramPacket(destination, destination.length, chatGroup, this.getLocalPort());

		super.send(packet);

	}

	public void receive(DatagramPacket packet) throws IOException {
		super.receive(packet);

        byte[] data, payload = null;
		do {
            data = packet.getData();
            byte version = data[0], type = data[2];

            if (version != VERSION) continue;

            if (type != MESSAGE_PAYLOAD) continue;

            short length = (short) (data[4] << 8 | data[5]);

            byte[] header = new byte[HEADER_SIZE];
            payload = new byte[length];

            System.arraycopy(data, 0, header, 0, HEADER_SIZE);
            System.arraycopy(data, HEADER_SIZE, payload, 0, length);

            payload = unlockPayload(payload);
        }while (payload == null);

		packet.setData(payload);
	}

	private byte[] makeHeader(byte payload_type, short payload_size){
		ByteBuffer header = ByteBuffer.allocate(6);

		header.put(VERSION);
		header.put(SEPARATOR);
		header.put(payload_type);
		header.put(SEPARATOR);
		header.putShort(payload_size);

		return header.array();
	}

	private byte[] makePayload(byte[] data, String username) throws IOException {

		try {
			cipher.init(Cipher.ENCRYPT_MODE, algorithmKey,
					new IvParameterSpec(ivBytes));
			mac.init(macKM);

			ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
			DataOutputStream dataStream = new DataOutputStream(byteStream);

			dataStream.writeUTF(username);
			long nonceSent = System.currentTimeMillis() * (int)( Math.random() * 100);
			dataStream.writeLong(nonceSent);
			dataStream.write(data, 0, data.length); //??
			dataStream.close();

			byte[] mp = byteStream.toByteArray();
			byte[] mpMac = mac.doFinal(mp);

			byteStream = new ByteArrayOutputStream();
			dataStream = new DataOutputStream(byteStream);
			dataStream.write(mp, 0, mp.length);
			dataStream.write(mpMac, 0, mpMac.length);
			dataStream.close();
			byte[] c = byteStream.toByteArray();

			mac.init(macKA);

			byte[] cCipher = cipher.doFinal(c);
			byte[] cMac = mac.doFinal(cCipher);

			byteStream = new ByteArrayOutputStream();
			dataStream = new DataOutputStream(byteStream);
			dataStream.write(cCipher, 0, cCipher.length);
			dataStream.write(cMac, 0, cMac.length);
			dataStream.close();

			return byteStream.toByteArray();

		} catch (InvalidKeyException e) {
			System.err.println("A chave e invalida.");
		} catch (InvalidAlgorithmParameterException e) {
			System.err.println("Parametros do algoritmo invalidos.");
		} catch (IllegalBlockSizeException e) {
			System.err.println("Tamanho de bloco invalido.");
		} catch (BadPaddingException e) {
			System.err.println("Preenchimento invalido.");
		}

		System.exit(1);
		return null;

	}

	private byte[] unlockPayload(byte[] data) throws IOException {
		try {
			cipher.init(Cipher.DECRYPT_MODE, algorithmKey,
					new IvParameterSpec(ivBytes));

			mac.init(macKA);

			byte[] payload = new byte[data.length - mac.getMacLength()];
			byte[] payloadMac = new byte[mac.getMacLength()];

			int offset = data.length - mac.getMacLength();
			System.arraycopy(data, 0, payload, 0, offset);
			System.arraycopy(data, offset, payloadMac, 0, mac.getMacLength());
			mac.update(payload);

			//testing C against MAC(C)
			if (!MessageDigest.isEqual(mac.doFinal(), payloadMac)) {
				return null; //panic
			}

			payload = cipher.doFinal(payload);

			mac.init(macKM);

			int length = mac.getMacLength();
			offset = payload.length - length;
			byte[] message = new byte[offset];
			byte[] messageMac = new byte[length];

			System.arraycopy(payload, 0, message, 0, offset);
			System.arraycopy(payload, offset,
					messageMac, 0, mac.getMacLength());
			mac.update(message);

			//testing MP against MAC(MP)
			if (!MessageDigest.isEqual(mac.doFinal(), messageMac)) {
				return null; //panic
			}


			DataInputStream dataStream = new DataInputStream(
					new ByteArrayInputStream(message, 0, message.length));

			String userName = dataStream.readUTF();

			long nonce = dataStream.readLong();
			//stopping replay attacks
			List<Long> tmp = nonceHistory.get(userName);
			if (tmp == null) {
				tmp = new LinkedList<>();
				((LinkedList<Long>) tmp).addFirst(nonce);
				nonceHistory.put(userName, tmp);
			} else {
				if (!tmp.contains(nonce)) {
					if (tmp.size() > HISTORY) {
						tmp.remove(0);
						tmp.add(nonce);
					}
				}//else its a replay attack, so ignore
			}
			//replay attacks foiled
			byte[] plainText = new byte[
			                            message.length - userName.length() - Long.SIZE/8];
			dataStream.read(plainText);

			return plainText;

		} catch (InvalidKeyException e) {
			System.err.println("A chave e invalida.");
		} catch (InvalidAlgorithmParameterException e) {
			System.err.println("Parametros do algoritmo invalidos.");
		} catch (IllegalBlockSizeException e) {
			System.err.println("Tamanho de bloco invalido.");
		} catch (BadPaddingException e) {
			System.err.println("Preenchimento invalido.");
		}

		System.exit(1);
		return null;
	}
}
